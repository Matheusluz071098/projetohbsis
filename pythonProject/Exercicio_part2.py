from Exercicio import CarrinhoDeCompras, Produto


carrinho = CarrinhoDeCompras()

p1 = Produto('Cerveja', 12)
p2 = Produto('Agua', 6)
p3 = Produto('Suco', 7)
p4 = Produto('Refrigerante', 8)


carrinho.inserir_produto(p1)
carrinho.inserir_produto(p2)
carrinho.inserir_produto(p3)
carrinho.inserir_produto(p4)

carrinho.lista_produto()
print(carrinho.soma_total())