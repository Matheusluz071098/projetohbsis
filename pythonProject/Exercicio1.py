#Tendo como dados de entrada a altura e o sexo de uma pessoa, construa um algoritmo que calcule seu peso ideal, utilizando as seguintes fórmulas:
#Para homens: (72.7*h) - 58
#Para mulheres: (62.1*h) - 44.7
#Onde: h = altura em metros
#Peça o peso da pessoa e informe se ela está acima ou abaixo do peso.

altura = float (input('Digite a sua altura: '))
sexo = input('Digite o seu sexo: F - Feminino   M - Masculino ')
peso = float (input('Digite seu peso: '))
peso_ideal = 0

if sexo == 'F' or sexo == 'f':
    peso_ideal = (62.1*altura) - 44.7
    print("O peso ideal é %.0f"%(peso_ideal))

if sexo == 'm' or sexo == 'M':
    peso_ideal = (72.7 * altura) - 58
    print("O peso ideal é %.0f" % (peso_ideal))

if peso > peso_ideal:
    print('Você está acima do peso ideal')
elif peso < peso_ideal:

    print('Você está abaixo do peso ideal')
else:
    print('Você está dentro do peso ideal')
